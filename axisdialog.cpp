#include "axisdialog.h"
#include "ui_axisdialog.h"

AxisDialog::AxisDialog (Axis* axis, DilatogramWidget* parent) :
	QDialog(parent),
	ui(new Ui::AxisDialog)
{
	ui->setupUi(this);

	this->axis = axis;
	dilatogram = parent;

	loadAxis ();
}

AxisDialog::~AxisDialog()
{
	delete ui;
}

void AxisDialog::loadAxis () {
	ui->axisTitle->setText(axis->getTitle());

	ui->maxValueBox->setValue(axis->getMax());
	ui->minValueBox->setValue(axis->getMin());

	ui->stepBox->setValue(axis->getGridStepValue());

	ui->valueRadio->setChecked(false);
	ui->percentRadio->setChecked(false);
	switch (axis->getGridStepType()) {
	case Axis::Value:
		ui->valueRadio->setChecked(true);
		break;
	case Axis::Percent:
		ui->percentRadio->setChecked(true);
		break;
	}
}

void AxisDialog::updateAxis()
{
	int stepType = Axis::Value;
	if (ui->valueRadio->isChecked()) stepType = Axis::Value;
	else if (ui->percentRadio->isChecked()) stepType = Axis::Percent;
	axis->setLimits(ui->maxValueBox->value(), ui->minValueBox->value());
	axis->setGrid(ui->stepBox->value(), stepType);
	dilatogram->update();
}

void AxisDialog::on_buttonBox_accepted()
{
	updateAxis();
	done(QDialog::Accepted);
}

void AxisDialog::on_buttonBox_rejected()
{
	done(QDialog::Rejected);
}

void AxisDialog::on_buttonBox_clicked (QAbstractButton* button)
{
	switch (ui->buttonBox->standardButton(button)) {
	case QDialogButtonBox::Reset:
		axis->resetLimits();
		loadAxis ();
		dilatogram->update();
		return;
	case QDialogButtonBox::Apply:
		updateAxis();
		return;
	default:
		return;
	}
}

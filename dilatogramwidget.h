#ifndef DILATOGRAMWIDGET_H
#define DILATOGRAMWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPointF>
#include <QString>
#include <list>
#include <QLine>
#include <QMouseEvent>
#include <QRubberBand>
#include <QFontMetrics>

#include "diagonalrubberband.h"
#include "dilatogrampoint.h"
#include "dilatogramdocument.h"
#include "axis.h"

class DilatogramWidget : public QWidget
{
	Q_OBJECT
public:
	explicit DilatogramWidget(QWidget *parent = 0);
	~DilatogramWidget();
	void setDocument(DilatogramDocument* pDoc);
	void paint(QPainter &painter);
	void setMouseAction(int mouseAction);
	void setVisibleCurves(int visibleCurves);
	bool setCoordinateSystems(int coordinateSystems, int dilatationUnit);
	void setLabel(QString label);

	enum {
		Zoom,
		Select,
		LineSelect
	} MOUSEACTION ;

	enum {
		Experiments = 0x1,
		Heating = 0x2,
		Cooling = 0x4,
		Derivative = 0x8,
		Approximation = 0x10
	} VISIBLECURVES ;

	enum {
		T_E,
		E_T,
		T_t,
		E_t,
		T_t_E_t,
		CSYSS
	} COORDINATESYSTEMS;

	enum  {
		DilatationUnitMicrometer,
		DilatationUnitPercent
	} DILATATIONUNIT;

signals:

public slots:

private:
	static const int Slit = 3;

	Axis* axes[DilatogramPoint::ParameterCount];
	std::list<Axis*> xDilatogramAxes;
	std::list<Axis*> yDilatogramAxes;
	std::list<Axis*> zDilatogramAxes;

	int mouseAction;
	int visibleCurves;
	int coordinateSystems;

	QPointF dragStart;
	QPointF dragEnd;
	bool isDragging;
	bool isLineDragging;

	QPainter painter;
	DilatogramDocument *pDoc;

	QString label;
	std::list<DilatogramPoint> approximation;
	std::list<DilatogramPoint> preparedReadout;
	std::list<QPointF> selectedPoints;

	int LBorder, RBorder, TBorder, BBorder;

	int curheight;
	int curwidth;

	QFontMetrics* fontMetricsDisplay;

	QPointF toValue(Axis* xAxis, Axis* yAxis, QPointF p);
	QPointF toPoint(Axis* xAxis, Axis* yAxis, QPointF p);
	QPointF toPoint(Axis* xAxis, Axis* yAxis, DilatogramPoint da);
	QPointF toPoint(Axis* xAxis, Axis* yAxis, float xDilatogramAxes, float yDilatogramAxes);
	QLine calcSelectLine (QPoint p1, QPoint p2);

	void updateDerivative();

	QRubberBand* zoomRect;
	QRubberBand* selectLineHorizontal;
	QRubberBand* selectLineVertical;
	DiagonalRubberBand* selectLineDiagonal;
protected:
	void paintEvent(QPaintEvent *event);
	void mouseMoveEvent (QMouseEvent* event);
	void mousePressEvent (QMouseEvent* event);
	void mouseReleaseEvent (QMouseEvent* event);
};

#endif // DILATOGRAMWIDGET_H

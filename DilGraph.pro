#-------------------------------------------------
#
# Project created by QtCreator 2014-11-13T14:36:35
#
#-------------------------------------------------

QT += core gui
QT += svg
QT += printsupport

TARGET = DilGraph
TEMPLATE = app

SOURCES += main.cpp\
		mainwindow.cpp \
	axisdialog.cpp \
	axis.cpp \
	config.cpp \
	configdialog.cpp \
	labeldialog.cpp \
	diagonalrubberband.cpp \
	dilatogramdocument.cpp \
	dilatogrampoint.cpp \
	dilatogramwidget.cpp

HEADERS  += mainwindow.h \
	axisdialog.h \
	axis.h \
	config.h \
	configdialog.h \
	labeldialog.h \
	diagonalrubberband.h \
	dilatogrampoint.h \
	dilatogramwidget.h \
	dilatogramdocument.h

FORMS    += mainwindow.ui \
	axisdialog.ui \
	configdialog.ui \
	labeldialog.ui

OTHER_FILES += \
	russian.ts

TRANSLATIONS = russian.ts

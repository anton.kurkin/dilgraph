#ifndef DILATOGRAMDOCUMENT_H
#define DILATOGRAMDOCUMENT_H

#include <list>
#include <QQueue>
#include <QString>
#include <QVector>
#include <cmath>

#include "config.h"
#include "dilatogrampoint.h"

class DilatogramDocument
{
private:

	Config* pConfig;
    std::list<QVector <float> > readout;
    std::list<DilatogramPoint> preparedReadout;
    std::list<DilatogramPoint> approximation;

	DilatogramPoint MovingLeastSquaresApproximation (QQueue <DilatogramPoint> sample, bool *ok = 0);
	DilatogramPoint MovingAverageApproximation (QQueue <DilatogramPoint> sample, bool *ok = 0);
	void MovingSample (DilatogramPoint (DilatogramDocument::*appf)(QQueue <DilatogramPoint> sample, bool *ok));

	QString label;
	float dilatationOffset;

	int readoutLine (QVector <float>* readoutVector, QString line, int n);
	inline float getReadoutParameter (QVector <float> readoutPoint, int i) {if (i < 0 || i >= readoutPoint.size()) return NAN; return readoutPoint[i];}

	inline float sensorValueToDilatation (float value) {return pConfig->getDilCoefficient() * value - dilatationOffset;}
	inline float dilatationMicrometerToPercent (float dilatationMicrometer) {return dilatationMicrometer / pConfig->getSampleSize();}

public:

	DilatogramDocument (Config* pConfig);

	void load (QString fileName);
	void prepare ();
	void approximate ();
	void save (QString fileName);

    inline std::list<DilatogramPoint> getPrepared () {return preparedReadout;}
    inline std::list<DilatogramPoint> getApproximation () {return approximation;}
	inline QString getLabel () {return label;}
};

#endif // DILATOGRAMDOCUMENT_H

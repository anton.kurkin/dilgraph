#include <QApplication>
#include <QTranslator>
#include "mainwindow.h"

#define RUSSIAN
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

#ifdef RUSSIAN
	QTranslator *translator = new QTranslator;
	translator->load("russian.qm");

	a.installTranslator(translator);
#endif

	MainWindow w;
	w.show();

	int ret = a.exec();

#ifdef RUSSIAN
	delete translator;
#endif

	return ret;
}

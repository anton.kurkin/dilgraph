<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU" sourcelanguage="en">
<context>
    <name>AxisDialog</name>
    <message>
        <location filename="axisdialog.ui" line="14"/>
        <source>Axis</source>
        <translation>Ось</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="20"/>
        <source>Axis Title</source>
        <translation>Название оси</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="43"/>
        <source>Min. value</source>
        <translation>Мин. значение</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="63"/>
        <source>Max. value</source>
        <translation>Макс. значение</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="99"/>
        <source>Step</source>
        <translation>Шаг</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="114"/>
        <source>Step type</source>
        <translation>Тип шага
</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="120"/>
        <source>Percent</source>
        <translation>Процент</translation>
    </message>
    <message>
        <location filename="axisdialog.ui" line="130"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>AxisTitle</name>
    <message>
        <location filename="axis.cpp" line="5"/>
        <source>Time, second</source>
        <oldsource>Time, s</oldsource>
        <translation>Время, с</translation>
    </message>
    <message>
        <location filename="axis.cpp" line="6"/>
        <source>Temperature, degrees Celsius</source>
        <oldsource>Temperature, Â°C</oldsource>
        <translation>Температура, °C</translation>
    </message>
    <message>
        <location filename="axis.cpp" line="7"/>
        <source>Dilatation, micro-meter</source>
        <oldsource>Dilatation, Âµm</oldsource>
        <translation>Дилатация, мкм</translation>
    </message>
    <message>
        <location filename="axis.cpp" line="8"/>
        <source>Dilatation, 10^-3 %</source>
        <oldsource>Dilatation, %</oldsource>
        <translation>Дилатация, 10^-3 %</translation>
    </message>
    <message>
        <location filename="axis.cpp" line="9"/>
        <source>Power, %</source>
        <translation>Мощность, %</translation>
    </message>
    <message>
        <location filename="axis.cpp" line="10"/>
        <source>dDilatation/dTime, micro-meter/second</source>
        <oldsource>dDilatation/dTime, Âµm/s</oldsource>
        <translation>dДилатация/dВремя, мкм/с</translation>
    </message>
    <message>
        <location filename="axis.cpp" line="11"/>
        <source>dDilatation/dTemperature, micro-meter/degrees-Celsius</source>
        <oldsource>dDilatation/dTemperature, Âµm/Â°C</oldsource>
        <translation>dДилатация/dТемпература, мкм/°C</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="14"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="33"/>
        <source>Approximation</source>
        <translation>Аппроксимация</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="44"/>
        <source>Temperature interval (cooling)</source>
        <translation>Температурный интервал (охлаждение)</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="64"/>
        <source>Temperature interval (heating)</source>
        <translation>Температурный интервал (нагрев)</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="80"/>
        <source>Approximation Method</source>
        <translation>Метод аппроксимации</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="92"/>
        <source>Moving Average</source>
        <translation>Скользящее среднее</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="99"/>
        <source>Moving Least Squares</source>
        <translation>Метод наименьших квадратов</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="113"/>
        <source>Preparation</source>
        <translation>Подготовка</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="197"/>
        <source>Min. spike dilatation change</source>
        <translation>Мин. выброс по дилатации</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="204"/>
        <source>Sample size, mm</source>
        <oldsource>Sample size, micrometer</oldsource>
        <translation>Размер образца, мм</translation>
    </message>
    <message>
        <source>Dilatation Scale Unit Type</source>
        <translation type="obsolete">Единица шкалы дилатации</translation>
    </message>
    <message>
        <source>Micrometer</source>
        <translation type="obsolete">Микрометр</translation>
    </message>
    <message>
        <source>Percent</source>
        <translation type="obsolete">Процент</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="121"/>
        <source>Temperature precision</source>
        <translation>Точность по температуре</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="190"/>
        <source>Dilatation Coefficient</source>
        <translation>Коэфициент дилатации</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="164"/>
        <source>Min. measurable dilatation</source>
        <translation>Мин. значение дилатации</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="227"/>
        <source>Readout</source>
        <translation>Считывание</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="240"/>
        <source>Number of columns</source>
        <translation>Количество колонок</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="261"/>
        <source>Planned temperature column #</source>
        <translation>Колонка планируемой температуры</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="268"/>
        <source>Time column #</source>
        <translation>Колонка времени</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="289"/>
        <source>Header size (lines)</source>
        <translation>Заголовок (количество строк)</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="303"/>
        <source>Dilatation column #</source>
        <translation>Колонка дилатации</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="310"/>
        <source>Real temperature column #</source>
        <translation>Колонка показаний термопары</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="317"/>
        <source>Power column #</source>
        <translation>Колонка мощности нагревателя</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="334"/>
        <source>Label line #</source>
        <oldsource>Description line #</oldsource>
        <translation>Строка подписи</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="84"/>
        <source>Load Config File</source>
        <translation>Загрузить файл конфигурации</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="84"/>
        <source>Config Files (*.cfg)</source>
        <translation>Файл конфигурации (*.cfg)</translation>
    </message>
</context>
<context>
    <name>LabelDialog</name>
    <message>
        <location filename="labeldialog.ui" line="14"/>
        <source>Dilatogram Label</source>
        <translation>Подпись дилатограммы</translation>
    </message>
    <message>
        <location filename="labeldialog.ui" line="20"/>
        <source>Dilatogram label</source>
        <translation>Подпись дилатограммы</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>DilGraph</source>
        <translation>ДилГраф</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="54"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="60"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>Toolbar</source>
        <oldsource>toolBar</oldsource>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="109"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Open...</source>
        <translation>Открыть...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="122"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="127"/>
        <source>Save...</source>
        <translation>Сохранить...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="135"/>
        <source>Propertes...</source>
        <translation>Свойства...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <source>Print...</source>
        <translation>Печать...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="191"/>
        <location filename="mainwindow.ui" line="194"/>
        <source>Experiment</source>
        <translation>Эксперимент</translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="249"/>
        <source>ε(T)</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.ui" line="268"/>
        <source>ε(t)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="279"/>
        <source>Approximation</source>
        <translation>Аппроксимация</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="287"/>
        <source>Line Select</source>
        <translation>Выбор Линии</translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="298"/>
        <source>10⁻³ %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <source>Dilatation scale unit - percent</source>
        <translation>Размерность шкалы дилатации - проценты</translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="309"/>
        <source>μm</source>
        <translation>мкм</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="312"/>
        <source>Dilatation scale unit - micro-meter</source>
        <translation>Размерность шкалы дилатации - микрометры</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <source>Reload</source>
        <translation>Перечитать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>Clean</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="153"/>
        <source>Approximate</source>
        <translation>Аппроксимировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Zoom</source>
        <translation>Лупа</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="180"/>
        <source>Select</source>
        <translation>Выбор</translation>
    </message>
    <message>
        <source>Experiments</source>
        <translation type="obsolete">Эксперименты</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="205"/>
        <source>Heating</source>
        <translation>Нагрев</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <source>Cooling</source>
        <translation>Охлаждение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="227"/>
        <source>Derivative</source>
        <translation>Производная</translation>
    </message>
    <message utf8="true">
        <location filename="mainwindow.ui" line="238"/>
        <location filename="mainwindow.ui" line="241"/>
        <source>T(ε)</source>
        <oldsource>T(Σα)</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="257"/>
        <source>T(t)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="57"/>
        <source>Load File</source>
        <translation>Загрузка файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="57"/>
        <source>Files (*.*)</source>
        <translation>Файлы (*.*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="72"/>
        <source>Save File</source>
        <translation>Сохранение файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="69"/>
        <source>SVG files (*.svg)</source>
        <oldsource>PNG files (*.png);;SVG files (*.svg)</oldsource>
        <translation>SVG файлы (*.svg)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="68"/>
        <source>PNG files (*.png)</source>
        <translation>PNG файлы (*.png)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="95"/>
        <source>Dilatogram</source>
        <translation>Дилатограмма</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="96"/>
        <source>Generated with DilGraph</source>
        <translation>Сгенерировано с помощью ДилГраф</translation>
    </message>
</context>
</TS>

#include "diagonalrubberband.h"
#include <QPainter>
#include <QPaintEvent>
#include <QPen>
#include <QStyleOptionRubberBand>

DiagonalRubberBand::DiagonalRubberBand(Shape s, QWidget * p) :
	QRubberBand(s, p)
{
	tlbr = false;
}

void DiagonalRubberBand::paintEvent(QPaintEvent *pe)
{
	QPainter painter;
	painter.begin(this);
	QPen pen (Qt::DashLine);
	painter.setPen(pen);
	if (tlbr)
		painter.drawLine (pe->rect().topLeft(), pe->rect().bottomRight());
	else
		painter.drawLine (pe->rect().bottomLeft(), pe->rect().topRight());
	painter.end();
}

void DiagonalRubberBand::setGeometry(QPoint s, QPoint e)
{
	if (s.x() > e.x()) {
		if (s.y() > e.y())
			tlbr = true;
		else
			tlbr = false;
	} else {
		if (s.y() > e.y())
			tlbr = false;
		else
			tlbr = true;
	}
	QRubberBand::setGeometry (QRect(s, e).normalized());
}

#include "config.h"

#include <QFile>
#include <QRegularExpression>
#include <QStringList>
#include <QStringListIterator>
#include <QTextStream>
#include <stdexcept>
#include <limits>

const Config::PropertyDescriptor Config::PropertyDescriptors[] = {
	{"SkipLines", Config::Int, Config::Readout},
	{"LabelLine", Config::Int, Config::Readout},
	{"ColN", Config::Int, Config::Readout},
	{"TimeCol", Config::Int, Config::Readout},
	{"PlannedTemperatureCol", Config::Int, Config::Readout},
	{"RealTemperatureCol", Config::Int, Config::Readout},
	{"DilatationCol", Config::Int, Config::Readout},
	{"PowerCol", Config::Int, Config::Readout},

	{"DilCoefficient", Config::Float, Config::Preparation},
	{"MinDil", Config::Float, Config::Preparation},
	{"TemperaturePrecision", Config::Float, Config::Preparation},
	{"DilatationChangeMax", Config::Float, Config::Preparation},
	{"SampleSize", Config::Float, Config::Preparation},

	{"ApproximationMethod", Config::Int, Config::Approximation},
	{"TemperatureIntervalCooling", Config::Float, Config::Approximation},
	{"TemperatureIntervalHeating", Config::Float, Config::Approximation},
	};

QHash <QString, int> Config::PropertyNameIndex;
Config::_init Config::_initializer;

const QString Config::PropertySubsectionNames[] = {"Readout", "Preparation", "Approximation"};
const int Config::PropertySubsectionCodes[] = {0x1, 0x2, 0x4};

const char Config::separator = ' ';
const char Config::assignment = '=';

Config::Config ()
{
	initDefault ();
	load ();
}

Config::Config (QString fileName)
{
	initDefault ();
	load (fileName);
}

void Config::load (QString fileName)
{
	this->fileName = fileName;
	load ();
}

void Config::save (QString fileName)
{
	this->fileName = fileName;
	save ();
}

void Config::initDefault() {
	//set default properties
	fileName = "config.cfg";
	updatedFlags = 0;

	// readout properties
	propertyValues[Config::SkipLines].IntValue = 3;
	propertyValues[Config::LabelLine].IntValue = 0;
	propertyValues[Config::ColN].IntValue = 7;
	propertyValues[Config::TimeCol].IntValue = 0;
	propertyValues[Config::PlannedTemperatureCol].IntValue = 1;
	propertyValues[Config::RealTemperatureCol].IntValue = 2;
	propertyValues[Config::DilatationCol].IntValue = 4;
	propertyValues[Config::PowerCol].IntValue = -1;

	// preparation properties
	propertyValues[Config::DilCoefficient].FloatValue = 1;
	propertyValues[Config::MinDil].FloatValue = 0;
	propertyValues[Config::TemperaturePrecision].FloatValue = 10;
	propertyValues[Config::DilatationChangeMax].FloatValue = 10;
	propertyValues[Config::SampleSize].FloatValue = 10;

	// approximation properties
	propertyValues[Config::ApproximationMethod].IntValue = Config::MovingLeastSquares;
	propertyValues[Config::TemperatureIntervalHeating].FloatValue = 30;
	propertyValues[Config::TemperatureIntervalCooling].FloatValue = 40;
}

void Config::load ()
{
	QFile file (fileName);

	if (!file.open(QFile::ReadOnly))
		return;


	QTextStream fin(&file);
	while ( !fin.atEnd() )
	{
		QString line = fin.readLine().trimmed();
		QStringList fieldsStr = line.split (QRegularExpression ("[ \\t]+"), Qt::SkipEmptyParts);
		if (fieldsStr.size() >= 3 && fieldsStr[1][0] == Config::assignment) { // {"ApproximationMethod", "=", "2"}
			int i = Config::PropertyNameIndex.value(fieldsStr[0], -1);
			if (i >= 0) {
				bool ok = true;
				switch (Config::PropertyDescriptors[i].type) {
				case Int: {
					int vali = fieldsStr[2].toInt(&ok);
					if (ok) setValue(i, vali);
				}
				break;
				case Float: {
					float valf = fieldsStr[2].toFloat(&ok);
					if (ok) setValue(i, valf);
				}
				break;
				}
			}
		}
	}
	file.close();
}

void Config::save ()
{
	QFile file (fileName);

	if (!file.open(QFile::WriteOnly | QFile::Text))
		return;

	QTextStream fout(&file);

	int subkey = -1;
	for (int i = 0; i < Config::PropertiesTotal; i++) {
		if (subkey != Config::PropertyDescriptors[i].subsection) {
			subkey = Config::PropertyDescriptors[i].subsection;
			fout << '\n' << '[' << Config::PropertySubsectionNames[subkey] << ']' << '\n';
		}

		fout << '\t' << Config::PropertyDescriptors[i].name << Config::separator << Config::assignment << Config::separator;

		switch (Config::PropertyDescriptors[i].type) {
		case Int:
			fout << propertyValues[i].IntValue;
			break;
		case Float:
			fout << propertyValues[i].FloatValue;
			break;
		}
		fout << '\n';
	}
	file.close();
}

void Config::setReadoutParms (int skipLines, int labelLine, int colN, int timeCol, int plannedTemperatureCol, int realTemperatureCol, int dilatationCol, int powerCol)
{
	setValue (Config::SkipLines, skipLines);
	setValue (Config::LabelLine, labelLine);
	setValue (Config::ColN, colN);
	setValue (Config::TimeCol, timeCol);
	setValue (Config::PlannedTemperatureCol, plannedTemperatureCol);
	setValue (Config::RealTemperatureCol, realTemperatureCol);
	setValue (Config::DilatationCol, dilatationCol);
	setValue (Config::PowerCol, powerCol);
}

void Config::setPreparationParms (float dilCoeff, float minDil, float temperaturePrecision, float dilatationChangeMax, float sampleSize)
{
	setValue (Config::DilCoefficient, dilCoeff);
	setValue (Config::MinDil, minDil);
	setValue (Config::TemperaturePrecision, temperaturePrecision);
	setValue (Config::DilatationChangeMax, dilatationChangeMax);
	setValue (Config::SampleSize, sampleSize);
}

void Config::setApproximationParms (int approximationMethod, float coolingTemperatureInterval, float heatingTemperatureInterval)
{
	setValue (Config::ApproximationMethod, approximationMethod);
	setValue (Config::TemperatureIntervalCooling, coolingTemperatureInterval);
	setValue (Config::TemperatureIntervalHeating, heatingTemperatureInterval);
}

bool Config::setValue (int property, int value)
{
	if (Config::PropertyDescriptors[property].type != Config::Int) return false;
	if (value == propertyValues[property].IntValue) return true;
	Config::updatedFlags |= Config::PropertySubsectionCodes[Config::PropertyDescriptors[property].subsection];
	propertyValues[property].IntValue = value;
	return true;
}

bool Config::setValue (int property, float value)
{
	if (Config::PropertyDescriptors[property].type != Config::Float) return false;
	if (value == propertyValues[property].FloatValue) return true;
	Config::updatedFlags |= Config::PropertySubsectionCodes[Config::PropertyDescriptors[property].subsection];
	propertyValues[property].FloatValue = value;
	return true;
}

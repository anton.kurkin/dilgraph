#include "axis.h"
#include <QApplication>
#include <cmath>

const char* Axis::axisTitles[] = { QT_TRANSLATE_NOOP("AxisTitle", "Time, second"),
								  QT_TRANSLATE_NOOP("AxisTitle", "Temperature, degrees Celsius"),
								  QT_TRANSLATE_NOOP("AxisTitle", "Dilatation, micro-meter"),
								  QT_TRANSLATE_NOOP("AxisTitle", "Dilatation, 10^-3 %"),
								  QT_TRANSLATE_NOOP("AxisTitle", "Power, %"),
								  QT_TRANSLATE_NOOP("AxisTitle", "dDilatation/dTime, micro-meter/second"),
								  QT_TRANSLATE_NOOP("AxisTitle", "dDilatation/dTemperature, micro-meter/degrees-Celsius") };

const QColor Axis::axisColors[][ColorCount] = {{QColor (200, 200, 200), QColor (200, 200, 200), QColor (200, 200, 200)},
											   {QColor (150,   0,   0), QColor (255, 150, 100), QColor (200, 100, 100)},
											   {QColor (  0, 100,   0), QColor ( 50, 250,  50), QColor ( 50, 200,  50)},
											   {QColor (  0, 100,   0), QColor ( 50, 250,  50), QColor ( 50, 200,  50)},
											   {QColor (  0,   0, 100), QColor (150, 150, 255), QColor (100, 100, 200)},
											   {QColor (100,   0, 100), QColor (200,  50, 200), QColor (150,  50, 150)},
											   {QColor (100,   0, 100), QColor (200,  50, 200), QColor (150,  50, 150)}};

Axis::Axis(int axisType)
{
	type = axisType;
	totalMin = 0;
	totalMax = 0;
	currentMin = 0;
	currentMax = 0;
	gridStepValue = 10;
	gridStepType = Axis::Percent;
	gridMin = 0;
	gridStep = 0;
}

float Axis::toPercent (float value)
{
	return (value - currentMin) / (currentMax - currentMin);
}

float Axis::toValue (float percent)
{
	return currentMin + percent * (currentMax - currentMin);
}

float Axis::interval ()
{
	return currentMax - currentMin;
}

QString	Axis::getTitle()
{
	return QApplication::translate("AxisTitle", Axis::axisTitles [type]);
}

QColor	Axis::getColor(int color)
{
	return Axis::axisColors [type][color];
}

void Axis::setGrid (float gridStepValue, int gridStepType)
{
	this->gridStepValue = gridStepValue;
	this->gridStepType = gridStepType;
	recalcGrid ();
}

void Axis::resetLimits ()
{
	currentMax = totalMax;
	currentMin = totalMin;
	recalcGrid ();
}

void Axis::setLimits (float currentMax, float currentMin)
{
	this->currentMax = currentMax;
	this->currentMin = currentMin;
	recalcGrid ();
}

void Axis::recalcGrid ()
{
	if (!isBigEnough()) return;
	switch (gridStepType) {
	case Axis::Percent:
		gridStep = interval() * gridStepValue / 100;
		gridMin = currentMin;
		break;
	case Axis::Value:
		gridStep = gridStepValue;
		gridMin = 0;
		if (currentMin > 0)
			while (gridMin < currentMin)
				gridMin += gridStep;
		else
			while (gridMin >= currentMin + gridStep - MinStep)
				gridMin -= gridStep;
		break;
	}

	float decade = 0.1;
	gridPrescision = 0;
	while (std::floor(gridStep * decade) == 0) {
		decade *= 10;
		gridPrescision++;
	}
}

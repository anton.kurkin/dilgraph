#ifndef DIAGONALRUBBERBAND_H
#define DIAGONALRUBBERBAND_H

#include <QRubberBand>
#include <QPoint>

class DiagonalRubberBand : public QRubberBand
{
private:
	bool tlbr; //topleft-bottomright if true bottomleft-topright if false
public:
	DiagonalRubberBand(Shape s, QWidget * p = 0);
	void setGeometry(QPoint s, QPoint e);
protected:
	void paintEvent(QPaintEvent *pe);
};

#endif // DIAGONALRUBBERBAND_H

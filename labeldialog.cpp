#include "labeldialog.h"
#include "ui_labeldialog.h"

LabelDialog::LabelDialog(QString label, DilatogramWidget *parent) :
	QDialog(parent),
	ui(new Ui::LabelDialog)
{
	ui->setupUi(this);
	ui->labelEdit->setText (label);
	dilatogram = parent;
}

LabelDialog::~LabelDialog()
{
	delete ui;
}

void LabelDialog::on_buttonBox_accepted()
{
	dilatogram->setLabel (ui->labelEdit->text());
}

#ifndef LABELDIALOG_H
#define LABELDIALOG_H

#include <QDialog>
#include "dilatogramwidget.h"

namespace Ui {
    class LabelDialog;
}

class LabelDialog : public QDialog
{
    Q_OBJECT
	DilatogramWidget* dilatogram;

public:
	explicit LabelDialog(QString label, DilatogramWidget *parent = 0);
    ~LabelDialog();

private slots:
	void on_buttonBox_accepted();

private:
    Ui::LabelDialog *ui;
};

#endif // LABELDIALOG_H

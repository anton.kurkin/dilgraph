#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>

#include "config.h"
#include "dilatogramdocument.h"
#include "config.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_actionPropertes_triggered();
	void on_actionExit_triggered();
	void on_actionOpen_triggered();
	void on_actionSave_triggered();
	void on_actionAbout_triggered();
	void on_actionPrint_triggered();

	void on_actionZoom_triggered(bool toggledOn);
	void on_actionSelect_triggered(bool toggledOn);
	void on_actionLineSelect_triggered(bool toggledOn);

	void on_actionHeating_triggered();
	void on_actionCooling_triggered();
	void on_actionExperiment_triggered();
	void on_actionApproximation_triggered();
	void on_actionDerivative_triggered();

	void on_actionT_Ea_triggered(bool toggledOn);
	void on_actionEa_T_triggered(bool toggledOn);
	void on_actionT_t_triggered(bool toggledOn);
	void on_actionEa_t_triggered(bool toggledOn);

	void on_action_um_triggered(bool toggledOn);
	void on_action_percent_triggered(bool toggledOn);

private:
	static const QString configFileName;
	Config* pConfig;
	DilatogramDocument* pDoc;
	Ui::MainWindow *ui;

	void changeVisibleCurves();

	int coordinateSystem;
	int dilatationUnit;

	void setDilatogramWidgetCoordinateSystem (int coordinateSystem);
	void setDilatogramWidgetDilatationUnit (int dilatationUnit);
};

#endif // MAINWINDOW_H

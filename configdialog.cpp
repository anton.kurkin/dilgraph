#include "configdialog.h"
#include "ui_configdialog.h"

#include <QFileDialog>

ConfigDialog::ConfigDialog(Config* pConfig, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ConfigDialog)
{
	ui->setupUi(this);

	this->pConfig = pConfig;
	loadConfig ();
}

ConfigDialog::~ConfigDialog()
{
	delete ui;
}

void ConfigDialog::loadConfig () {
	ui->headerSizeBox->setValue(pConfig->getSkipLines());
	ui->labelLineBox->setValue(pConfig->getLabelLine() +1);
	ui->columnNBox->setValue(pConfig->getColN());
	ui->timeColBox->setValue(pConfig->getTimeCol() +1);
	ui->plannedTemperatureColBox->setValue(pConfig->getPlannedTemperatureCol() +1);
	ui->realTemperatureColBox->setValue(pConfig->getRealTemperatureCol() +1);
	ui->dilatationColBox->setValue(pConfig->getDilatationCol() +1);
	ui->powerColBox->setValue(pConfig->getPowerCol() +1);

	ui->dilatationCoefficientBox->setValue(pConfig->getDilCoefficient());
	ui->minMeasurableDilatatioBox->setValue(pConfig->getMinDil());
	ui->temperaturePrecisionBox->setValue(pConfig->getTemperaturePrecision());
	ui->minSpikeDilatationChangeBox->setValue(pConfig->getDilatationChangeMax());
	ui->sampleSizeBox->setValue(pConfig->getSampleSize());

	ui->heatingTemperatureIntervalBox->setValue(pConfig->getTemperatureIntervalHeating());
	ui->coolingTemperatureIntervalBox->setValue(pConfig->getTemperatureIntervalCooling());

	ui->leastSquaresRadio->setChecked(false);
	ui->movingAverageRadio->setChecked(false);
	switch (pConfig->getApproximationMethod()) {
	case Config::MovingLeastSquares:
		ui->leastSquaresRadio->setChecked(true);
		break;
	case Config::MovingAverage:
		ui->movingAverageRadio->setChecked(true);
		break;
	}
}

void ConfigDialog::updateConfig () {
	pConfig->setReadoutParms (ui->headerSizeBox->value(), ui->labelLineBox->value() -1, ui->columnNBox->value(),
							 ui->timeColBox->value() -1, ui->plannedTemperatureColBox->value() -1, ui->realTemperatureColBox->value() -1, ui->dilatationColBox->value() -1, ui->powerColBox->value() -1);

	pConfig->setPreparationParms (ui->dilatationCoefficientBox->value(), ui->minMeasurableDilatatioBox->value(), ui->temperaturePrecisionBox->value(), ui->minSpikeDilatationChangeBox->value(), ui->sampleSizeBox->value());

	int approximatonMethod = Config::MovingLeastSquares;
	if (ui->leastSquaresRadio->isChecked()) approximatonMethod = Config::MovingLeastSquares;
	else if (ui->movingAverageRadio->isChecked()) approximatonMethod = Config::MovingAverage;

	pConfig->setApproximationParms (approximatonMethod, ui->coolingTemperatureIntervalBox->value(), ui->heatingTemperatureIntervalBox->value());
}

void ConfigDialog::on_buttonBox_accepted()
{
	updateConfig ();
	done(QDialog::Accepted);
}

void ConfigDialog::on_buttonBox_rejected()
{
	done(QDialog::Rejected);
}

void ConfigDialog::on_buttonBox_clicked (QAbstractButton* button)
{
	switch (ui->buttonBox->standardButton(button)) {
	case QDialogButtonBox::Reset:
		pConfig->load ();
		loadConfig ();
		return;
	case QDialogButtonBox::Open: {
		QString fileName = QFileDialog::getOpenFileName(this, tr("Load Config File"), "", tr("Config Files (*.cfg)"));
		if (fileName.isEmpty())
			return;
		pConfig->load (fileName);
		loadConfig ();
		return;
	}
	case QDialogButtonBox::Save:
		updateConfig ();
		pConfig->save ();
		done(QDialog::Accepted);
	default:
		return;
	}
}

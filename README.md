# DilGraph

DilGraph is a tool to process text data files from experiments that are being obtained from different dilatometers (2 types during the development). That includes drawing dilatogram and time-based graphs and selection of key points/cooling/heating speed

## Contributing.

Development is discontinued.

## License.

[MIT](https://choosealicense.com/licenses/mit/)


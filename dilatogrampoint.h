#ifndef DILATOGRAMPOINT_H
#define DILATOGRAMPOINT_H
#include <cmath>

class DilatogramPoint
{
public:
	enum {
		Time,
		Temperature,
		DilatationMicrometer,
		DilatationPercent,
		Power,
		dDilatation_dTime,
		dDilatation_dTemperature,
		ParameterCount
	} POINTPARAMETERS;

private:
	float parameters [DilatogramPoint::ParameterCount];
	bool isHeating;
public:
	DilatogramPoint();

	void set(float time, float temperature, float dilatationmicrometer, float dilatationpercent, float power, float ddilatation_dtime, float ddilatation_dtemperature, bool isHeating);
	inline void set () {set (NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN);}
	inline void set (float time, float temperature, float dilatationmicrometer, float dilatationpercent, float power, bool isHeating) { set (time, temperature, dilatationmicrometer, dilatationpercent, power, NAN, NAN, isHeating); }

	inline float getTime() const {return parameters [DilatogramPoint::Time];}
	inline float getTemperature() const {return parameters [DilatogramPoint::Temperature];}
	inline float getDilatationMicrometer() const {return parameters [DilatogramPoint::DilatationMicrometer];}
	inline float getDilatationPercent() const {return parameters [DilatogramPoint::DilatationPercent];}
	inline float getPower() const {return parameters [DilatogramPoint::Power];}
	inline float getdDilatationdTemperaure() const {return parameters [DilatogramPoint::dDilatation_dTemperature];}
	inline float get (int i) const {return parameters [i];}

	inline bool heating() const {return isHeating;}
};

#endif // DILATOGRAMPOINT_H

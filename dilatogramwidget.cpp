#include "dilatogramwidget.h"
#include <list>
#include <limits>
#include <cmath>

#include "axisdialog.h"
#include "labeldialog.h"

DilatogramWidget::DilatogramWidget(QWidget *parent) :
	QWidget(parent)
{
	setBackgroundRole(QPalette::Base);

	LBorder = 100;
	RBorder = 100;
	TBorder = 20;
	BBorder = 40;

	for (int i = 0; i < DilatogramPoint::ParameterCount; i++)
		axes[i] = new Axis(i);

	axes[DilatogramPoint::Temperature]->setGrid(100, Axis::Value);
	axes[DilatogramPoint::DilatationMicrometer]->setGrid(20, Axis::Value);
	axes[DilatogramPoint::Time]->setGrid(10, Axis::Value);

	isDragging = false;
	isLineDragging = false;

	setMouseTracking(true);

	zoomRect = new QRubberBand(QRubberBand::Rectangle, this);
	zoomRect->hide();

	selectLineHorizontal = new QRubberBand(QRubberBand::Line, this);
	selectLineHorizontal->hide();
	selectLineVertical = new QRubberBand(QRubberBand::Line, this);
	selectLineVertical->hide();
	selectLineDiagonal = new DiagonalRubberBand(QRubberBand::Line, this);
	selectLineVertical->hide();

	mouseAction = DilatogramWidget::Zoom;
	visibleCurves = DilatogramWidget::Heating | DilatogramWidget::Cooling | DilatogramWidget::Experiments | DilatogramWidget::Approximation | DilatogramWidget::Derivative;

	coordinateSystems = -1;
	setCoordinateSystems (DilatogramWidget::T_E, DilatogramWidget::DilatationUnitPercent);

	fontMetricsDisplay = NULL;
}

DilatogramWidget::~DilatogramWidget()
{
	if (fontMetricsDisplay != NULL)
		delete fontMetricsDisplay;

	delete selectLineDiagonal;
	delete selectLineVertical;
	delete selectLineHorizontal;
	delete zoomRect;

	for (int i = DilatogramPoint::ParameterCount-1; i >= 0 ; i--)
		delete axes[i];
}

QLine DilatogramWidget::calcSelectLine (QPoint p1, QPoint p2) {
	float k = (float(p1.y() - p2.y())) / (p1.x() - p2.x());
	float b = p1.y() - k * p1.x();
	int x1, y1, x2, y2;

	y1 = TBorder;
	x1 = (y1 - b) / k;
	if (x1 < LBorder) {
		x1 = LBorder;
		y1 = k * x1 + b;
	} else if (x1 > width() - RBorder) {
		x1 = width() - RBorder;
		y1 = k * x1 + b;
	}

	y2 = height() - BBorder;
	x2 = (y2 - b) / k;
	if (x2 < LBorder) {
		x2 = LBorder;
		y2 = k * x2 + b;
	} else if (x2 > width() - RBorder) {
		x2 = width() - RBorder;
		y2 = k * x2 + b;
	}

	return QLine(x1, y1, x2, y2);
}

void DilatogramWidget::setMouseAction(int mouseAction)
{
	this->mouseAction = mouseAction;
	isDragging = false;
}

void DilatogramWidget::setVisibleCurves(int visibleCurves)
{
	this->visibleCurves = visibleCurves;
	updateDerivative();
	update();
}

void DilatogramWidget::setLabel(QString label)
{
	this->label = label;
	update();
}

bool DilatogramWidget::setCoordinateSystems(int coordinateSystems, int dilatationUnit)
{
	bool coordinateSystemsChanged = true;
	if (this->coordinateSystems == coordinateSystems)
		coordinateSystemsChanged = false;
	else
		this->coordinateSystems = coordinateSystems;
	xDilatogramAxes.clear();
	yDilatogramAxes.clear();
	zDilatogramAxes.clear();

	int DilatationSelect = DilatogramPoint::DilatationPercent;
	switch (dilatationUnit) {
	case DilatogramWidget::DilatationUnitMicrometer:
		DilatationSelect = DilatogramPoint::DilatationMicrometer;
		break;
	case DilatogramWidget::DilatationUnitPercent:
		DilatationSelect = DilatogramPoint::DilatationPercent;
		break;
	}

	switch (coordinateSystems) {
	case DilatogramWidget::E_T:
		xDilatogramAxes.push_back(axes[DilatogramPoint::Temperature]);
		yDilatogramAxes.push_back(axes[DilatationSelect]);
		zDilatogramAxes.push_back(axes[DilatogramPoint::Time]);
		break;
	case DilatogramWidget::T_E:
		xDilatogramAxes.push_back(axes[DilatationSelect]);
		yDilatogramAxes.push_back(axes[DilatogramPoint::Temperature]);
		zDilatogramAxes.push_back(axes[DilatogramPoint::Time]);
		break;
	case DilatogramWidget::E_t:
		xDilatogramAxes.push_back(axes[DilatogramPoint::Time]);
		yDilatogramAxes.push_back(axes[DilatationSelect]);
		break;
	case DilatogramWidget::T_t:
		xDilatogramAxes.push_back(axes[DilatogramPoint::Time]);
		yDilatogramAxes.push_back(axes[DilatogramPoint::Temperature]);
		break;
	case DilatogramWidget::T_t_E_t:
		xDilatogramAxes.push_back(axes[DilatogramPoint::Time]);
		yDilatogramAxes.push_back(axes[DilatogramPoint::Temperature]);
		yDilatogramAxes.push_back(axes[DilatationSelect]);
		break;
	default:
		xDilatogramAxes.push_back(axes[DilatogramPoint::Temperature]);
		yDilatogramAxes.push_back(axes[DilatationSelect]);
		zDilatogramAxes.push_back(axes[DilatogramPoint::Time]);
	}
	updateDerivative();
	selectedPoints.clear();
	update();
	return coordinateSystemsChanged;
}

void DilatogramWidget::updateDerivative ()
{
	switch (coordinateSystems) {
	case DilatogramWidget::E_T:
		if (yDilatogramAxes.back()->getType() != DilatogramPoint::dDilatation_dTemperature) {
			if (visibleCurves & DilatogramWidget::Derivative)
				yDilatogramAxes.push_back (axes[DilatogramPoint::dDilatation_dTemperature]);
		} else {
			if (~visibleCurves & DilatogramWidget::Derivative)
				yDilatogramAxes.pop_back();
		}
		break;
	case DilatogramWidget::T_E:
		if (xDilatogramAxes.back()->getType() != DilatogramPoint::dDilatation_dTemperature) {
			if (visibleCurves & DilatogramWidget::Derivative)
				xDilatogramAxes.push_back (axes[DilatogramPoint::dDilatation_dTemperature]);
		} else {
			if (~visibleCurves & DilatogramWidget::Derivative)
				xDilatogramAxes.pop_back();
		}
		break;
	case DilatogramWidget::E_t:
	case DilatogramWidget::T_t_E_t:
		if (yDilatogramAxes.back()->getType() != DilatogramPoint::dDilatation_dTime) {
			if (visibleCurves & DilatogramWidget::Derivative)
				yDilatogramAxes.push_back (axes[DilatogramPoint::dDilatation_dTime]);
		} else {
			if (~visibleCurves & DilatogramWidget::Derivative)
				yDilatogramAxes.pop_back();
		}
		break;
	case DilatogramWidget::T_t:
		if (yDilatogramAxes.back()->getType() != DilatogramPoint::Power) {
			if (visibleCurves & DilatogramWidget::Derivative)
				yDilatogramAxes.push_back (axes[DilatogramPoint::Power]);
		} else {
			if (~visibleCurves & DilatogramWidget::Derivative)
				yDilatogramAxes.pop_back();
		}
		break;
	}
}

void DilatogramWidget::mouseMoveEvent (QMouseEvent* event)
{
	if (event->buttons() & Qt::LeftButton) {
		if (mouseAction == DilatogramWidget::Zoom)
			zoomRect->setGeometry(QRect(dragStart.toPoint(), event->pos()).normalized());
		if (mouseAction == DilatogramWidget::Select || mouseAction == DilatogramWidget::LineSelect) {
			selectLineHorizontal->setGeometry(LBorder, event->pos().y(), width() - LBorder - RBorder, 1);
			selectLineVertical->setGeometry(event->pos().x(), TBorder, 1, height() - TBorder - BBorder);
		}
	}
	if (mouseAction == DilatogramWidget::LineSelect && isLineDragging) {
		QLine diag = calcSelectLine (dragStart.toPoint(), event->pos());
		selectLineDiagonal->setGeometry(diag.p1(), diag.p2());
	}
}

void DilatogramWidget::mousePressEvent (QMouseEvent* event)
{
	switch (mouseAction) {
	case DilatogramWidget::Zoom:
		if (event->buttons() & Qt::LeftButton) {
			if (event->pos().y() < TBorder) {
				LabelDialog *dialog = new LabelDialog (label, this);
				dialog->show();
			} else if (event->pos().x() < LBorder) {
				if (height() - event->pos().y() < BBorder) {
					if (zDilatogramAxes.size()) {
						AxisDialog *dialog = new AxisDialog (zDilatogramAxes.front(), this);
						dialog->show();
						return;
					}
				} else {
					std::list<Axis*>::iterator yAxesIt = yDilatogramAxes.begin();
					float xOffset = 0;
					while (yAxesIt != yDilatogramAxes.end()) {
						Axis* yAxis = *(yAxesIt++);
						if (!yAxis->isBigEnough()) continue;
						xOffset += Slit + fontMetricsDisplay->height() + std::max (fontMetricsDisplay->horizontalAdvance (QString::number(yAxis->getGridMax(), 'f', yAxis->getGridPrescision())),
																				  fontMetricsDisplay->horizontalAdvance (QString::number(yAxis->getGridMin(), 'f', yAxis->getGridPrescision())));
						if (event->pos().x() > LBorder - xOffset) {
							AxisDialog *dialog = new AxisDialog (yAxis, this);
							dialog->show();
							return;
						}
					}
				}
			} else if (height() - event->pos().y() < BBorder) {
				std::list<Axis*>::iterator xAxesIt = xDilatogramAxes.begin();
				float yOffset = 0;
				while (xAxesIt != xDilatogramAxes.end()) {
					Axis* xAxis = *(xAxesIt++);
					if (!xAxis->isBigEnough()) continue;
					yOffset += Slit + fontMetricsDisplay->height() *2;
					if (height() - event->pos().y() > BBorder - yOffset) {
						AxisDialog *dialog = new AxisDialog (xAxis, this);
						dialog->show();
						return;
					}
				}
			} else {
				dragStart = event->pos();
				isDragging = true;

				zoomRect->setGeometry(QRect(dragStart.toPoint(), QSize()));
				zoomRect->show();
			}
		}
		if (event->buttons() & Qt::RightButton) {
			if (isDragging) {
				isDragging = false;
				zoomRect->hide();
			} else {
				xDilatogramAxes.front()->resetLimits();
				yDilatogramAxes.front()->resetLimits();

				update();
			}
		}
		return;

	case DilatogramWidget::Select:
		if (event->buttons() & Qt::LeftButton) {
			selectLineHorizontal->setGeometry(LBorder, event->pos().y(), width() - LBorder - RBorder, 1);
			selectLineHorizontal->show();
			selectLineVertical->setGeometry(event->pos().x(), TBorder, 1, height() - TBorder - BBorder);
			selectLineVertical->show();
			isDragging = true;
		}
		if (event->buttons() & Qt::RightButton) {
			if (isDragging) {
				isDragging = false;
				selectLineHorizontal->hide();
				selectLineVertical->hide();
			} else {
				QPointF p = event->pos();
				QPointF pTEa = toValue (xDilatogramAxes.front(), yDilatogramAxes.front(), p);

				float yPrec = 6* yDilatogramAxes.front()->interval() / (height() - DilatogramWidget::TBorder - DilatogramWidget::BBorder);
				float xPrec = 6* xDilatogramAxes.front()->interval() / (width() - DilatogramWidget::RBorder - DilatogramWidget::LBorder);

				for (std::list<QPointF>::iterator it = selectedPoints.begin(); it != selectedPoints.end(); it++) {
					if (std::abs(pTEa.y() - (*it).y()) <= yPrec && std::abs(pTEa.x() - (*it).x()) <= xPrec) {
						selectedPoints.erase(it);
						break;
					}
				}
				update();
			}
		}
		return;
	case DilatogramWidget::LineSelect:
		if (event->buttons() & Qt::LeftButton) {
			selectLineHorizontal->setGeometry(LBorder, event->pos().y(), width() - LBorder - RBorder, 1);
			selectLineHorizontal->show();
			selectLineVertical->setGeometry(event->pos().x(), TBorder, 1, height() - TBorder - BBorder);
			selectLineVertical->show();
			isDragging = true;
		}
		if (event->buttons() & Qt::RightButton) {
		}
		return;
	}
}

void DilatogramWidget::mouseReleaseEvent (QMouseEvent* event)
{
	if (event->button() == Qt::LeftButton && mouseAction == DilatogramWidget::Select && isDragging) {
		selectLineHorizontal->hide();
		selectLineVertical->hide();
		isDragging = false;
		QPointF p = event->pos();

		QPointF pTEa = toValue (xDilatogramAxes.front(), yDilatogramAxes.front(), p);
		selectedPoints.push_back(pTEa);
		update();
	}
	if (event->button() == Qt::LeftButton && mouseAction == DilatogramWidget::Zoom) {
		dragEnd = event->pos();
		zoomRect->hide();

		if (!isDragging || yDilatogramAxes.front()->interval() == 0 || xDilatogramAxes.front()->interval() == 0 || dragEnd == dragStart)
			return;

		isDragging = false;

		QPointF start = toValue (xDilatogramAxes.front(), yDilatogramAxes.front(), dragStart);
		QPointF end = toValue (xDilatogramAxes.front(), yDilatogramAxes.front(), dragEnd);

		xDilatogramAxes.front()->setLimits(std::max (start.x(), end.x()), std::min (start.x(), end.x()));
		yDilatogramAxes.front()->setLimits(std::max (start.y(), end.y()), std::min (start.y(), end.y()));

		update();
	}
	if (event->button() == Qt::LeftButton && mouseAction == DilatogramWidget::LineSelect) {
		selectLineHorizontal->hide();
		selectLineVertical->hide();
		if (!isLineDragging)
			dragStart = event->pos();
		else
			dragEnd = event->pos();
		isLineDragging = !isLineDragging;
		selectLineDiagonal->setVisible(isLineDragging);
		selectLineDiagonal->setGeometry(dragStart.toPoint(), dragStart.toPoint());
		update();
	}
}

void DilatogramWidget::setDocument(DilatogramDocument* pDoc)
{
	this->pDoc = pDoc;
	label = pDoc->getLabel();
	preparedReadout = pDoc->getPrepared();
	approximation = pDoc->getApproximation();

	float minParm[DilatogramPoint::ParameterCount] = {std::numeric_limits<float>::max()};
	float maxParm[DilatogramPoint::ParameterCount] = {-std::numeric_limits<float>::max()};

	std::list<DilatogramPoint>::iterator prepIt = preparedReadout.begin();
	while (prepIt != preparedReadout.end()) {
		DilatogramPoint da = *(prepIt++);

		for (int i = 0; i < DilatogramPoint::ParameterCount; i++) {
			if (da.get(i) > maxParm[i])
				maxParm[i] = da.get(i);
			if (da.get(i) < minParm[i])
				minParm[i] = da.get(i);
		}
	}

	std::list<DilatogramPoint>::iterator appIt = approximation.begin();
	while (appIt != approximation.end()) {
		DilatogramPoint da = *(appIt++);

		for (int i = 0; i < DilatogramPoint::ParameterCount; i++) {
			if (da.get(i) > maxParm[i])
				maxParm[i] = da.get(i);
			if (da.get(i) < minParm[i])
				minParm[i] = da.get(i);
		}
	}

	for (int i = 0; i < DilatogramPoint::ParameterCount; i++) {
		axes[i]->setLimits (maxParm[i], minParm[i]);
		axes[i]->updateTotalLimits();
	}

	axes[DilatogramPoint::Temperature]->setLimits (maxParm[DilatogramPoint::Temperature], std::min (0.f, minParm[DilatogramPoint::Temperature]));
	axes[DilatogramPoint::Temperature]->updateTotalLimits();

	selectedPoints.clear();
	update();
}

void DilatogramWidget::paintEvent(QPaintEvent *)
{
	painter.begin(this);
	paint(painter);
	painter.end();
}

QPointF DilatogramWidget::toValue(Axis* xAxis, Axis* yAxis, QPointF p)
{
	float xPerc = (p.x() - LBorder) / curwidth;
	float yPerc = ((height() - BBorder) - p.y()) / curheight;

	return QPointF(xAxis->toValue(xPerc), yAxis->toValue(yPerc));
}

QPointF DilatogramWidget::toPoint(Axis* xAxis, Axis* yAxis, QPointF p)
{
	return toPoint (xAxis, yAxis, p.x(), p.y());
}

QPointF DilatogramWidget::toPoint(Axis* xAxis, Axis* yAxis, DilatogramPoint da)
{
	float x = da.get(xAxis->getType());
	float y = da.get(yAxis->getType());

	return toPoint (xAxis, yAxis, x, y);
}

QPointF DilatogramWidget::toPoint(Axis* xAxis, Axis* yAxis, float x, float y)
{
	float xPerc = xAxis->toPercent(x);
	float yPerc = yAxis->toPercent(y);

	return QPointF(curwidth * xPerc, -curheight * yPerc);
}

void drawText(QPainter & painter, const QPointF & point, int flags,
			  const QString & text, float degrees = 0, QRectF * boundingRect = 0)
{
	const qreal size = std::numeric_limits<int>::max();
	double radians = degrees * M_PI / 180;
	QPointF pointRot (point.x(), point.y());
	pointRot.rx() = point.x() * cos(radians) - point.y() * sin(radians);
	pointRot.ry() = -point.x() * sin(radians) + point.y() * cos(radians);

	QPointF corner(pointRot.x(), pointRot.y() - size);
	if (flags & Qt::AlignHCenter) corner.rx() -= size/2.0;
	else if (flags & Qt::AlignRight) corner.rx() -= size;
	if (flags & Qt::AlignVCenter) corner.ry() += size/2.0;
	else if (flags & Qt::AlignTop) corner.ry() += size;
	else flags |= Qt::AlignBottom;

	QRectF rect(corner, QSizeF(size, size));
	painter.rotate(degrees);
	painter.drawText(rect, flags, text, boundingRect);
	painter.rotate(-degrees);
}

void drawText(QPainter & painter, qreal x, qreal y, int flags,
			  const QString & text, float degrees = 0, QRectF * boundingRect = 0)
{
	drawText(painter, QPointF(x,y), flags, text, degrees, boundingRect);
}

void DilatogramWidget::paint(QPainter &painter)
{
	// cleaning
	painter.fillRect(QRect(0, 0, width(), height()), Qt::white);

	if (fontMetricsDisplay == NULL)
		fontMetricsDisplay = new QFontMetrics (painter.font());

	LBorder = 0;
	BBorder = 0;
	TBorder = fontMetricsDisplay->height();

	std::list<Axis*>::iterator xAxesIt = xDilatogramAxes.begin();
	while (xAxesIt != xDilatogramAxes.end()) {
		Axis* xAxis = *(xAxesIt++);
		if (!xAxis->isBigEnough()) continue;
		BBorder += Slit + fontMetricsDisplay->height() *2;
	}

	std::list<Axis*>::iterator yAxesIt = yDilatogramAxes.begin();
	while (yAxesIt != yDilatogramAxes.end()) {
		Axis* yAxis = *(yAxesIt++);
		if (!yAxis->isBigEnough()) continue;
		LBorder += Slit + fontMetricsDisplay->height()
				   + std::max (fontMetricsDisplay->horizontalAdvance (QString::number(yAxis->getGridMax(), 'f', yAxis->getGridPrescision())),
							  fontMetricsDisplay->horizontalAdvance (QString::number(yAxis->getGridMin(), 'f', yAxis->getGridPrescision())));
	}

	drawText (painter, width() /2, 0, Qt::AlignHCenter | Qt::AlignTop, label);

	painter.translate(LBorder, height() - BBorder);

	// ----------------------------------------------------------------------------------------
	// grid on graph
	curheight = height() - TBorder - BBorder;
	curwidth = width() - LBorder - RBorder;

	painter.setPen (QPen (Qt::black, 1, Qt::DotLine));
	//vertical grid
	if (xDilatogramAxes.front()->isBigEnough()) {
		for (float i = xDilatogramAxes.front()->getGridMin(); i <= xDilatogramAxes.front()->getGridMax(); i += xDilatogramAxes.front()->getGridStep()) {
			float ireal = curwidth * xDilatogramAxes.front()->toPercent(i);
			painter.drawLine (ireal, 0, ireal, -curheight);
		}
	}
	//horizontal grid
	if (yDilatogramAxes.front()->isBigEnough()) {
		for (float i = yDilatogramAxes.front()->getGridMin(); i <= yDilatogramAxes.front()->getGridMax(); i += yDilatogramAxes.front()->getGridStep()) {
			float ireal = -curheight * yDilatogramAxes.front()->toPercent(i);
			painter.drawLine (0, ireal, curwidth, ireal);
		}
	}

	painter.setPen (QPen (Qt::black, 1, Qt::SolidLine));

	xAxesIt = xDilatogramAxes.begin();
	float yOffset = 0;
	while (xAxesIt != xDilatogramAxes.end()) {
		Axis* xAxis = *(xAxesIt++);
		if (!xAxis->isBigEnough()) continue;
		painter.drawLine (0, yOffset, curwidth, yOffset);
		yOffset += Slit;

		for (float i = xAxis->getGridMin(); i <= xAxis->getGridMax(); i += xAxis->getGridStep()) {
			float ireal = curwidth * xAxis->toPercent(i);
			painter.drawLine (ireal, yOffset, ireal, yOffset - Slit);
			drawText(painter, ireal, yOffset, Qt::AlignHCenter | Qt::AlignTop, QString::number(i, 'f', xAxis->getGridPrescision()));
		}
		yOffset += fontMetricsDisplay->height();
		drawText (painter, curwidth /2, yOffset, Qt::AlignHCenter | Qt::AlignTop, xAxis->getTitle(), 0);
		yOffset += fontMetricsDisplay->height();
	}

	yAxesIt = yDilatogramAxes.begin();
	float xOffset = 0;
	while (yAxesIt != yDilatogramAxes.end()) {
		Axis* yAxis = *(yAxesIt++);
		if (!yAxis->isBigEnough()) continue;
		painter.drawLine (xOffset, 0, xOffset, -curheight);
		xOffset -= Slit;

		for (float i = yAxis->getGridMin(); i <= yAxis->getGridMax(); i += yAxis->getGridStep()) {
			float ireal = -curheight * yAxis->toPercent(i);
			painter.drawLine (xOffset, ireal, xOffset + Slit, ireal);
			drawText (painter, xOffset, ireal, Qt::AlignRight | Qt::AlignVCenter, QString::number(i, 'f', yAxis->getGridPrescision()));
		}
		xOffset -= std::max (fontMetricsDisplay->horizontalAdvance (QString::number(yAxis->getGridMax(), 'f', yAxis->getGridPrescision())),
							fontMetricsDisplay->horizontalAdvance (QString::number(yAxis->getGridMin(), 'f', yAxis->getGridPrescision())));
		drawText (painter, xOffset, curheight /2, Qt::AlignHCenter | Qt::AlignTop, yAxis->getTitle(), 90);
		xOffset -= fontMetricsDisplay->height();
	}



	if (visibleCurves & DilatogramWidget::Heating || visibleCurves & DilatogramWidget::Cooling) {

		xAxesIt = xDilatogramAxes.begin();
		while (xAxesIt != xDilatogramAxes.end()) {
			Axis* xAxis = *(xAxesIt++);

			yAxesIt = yDilatogramAxes.begin();
			while (yAxesIt != yDilatogramAxes.end()) {
				Axis* yAxis = *(yAxesIt++);

				if (visibleCurves & DilatogramWidget::Experiments) {
					painter.setPen(QPen(yAxis->getColor(Axis::ExperimentColor), 4));
					std::list<DilatogramPoint>::iterator prepIt = preparedReadout.begin();
					while (prepIt != preparedReadout.end()) {
						DilatogramPoint da = *(prepIt++);
						bool show = true;
						if (!(da.heating() && (visibleCurves & DilatogramWidget::Heating)) && !(!da.heating() && (visibleCurves & DilatogramWidget::Cooling)))
							show = false;
						for (int i = 0; i < DilatogramPoint::ParameterCount && show; i++)
							if (da.get(i) != NAN && (da.get(i) > axes[i]->getMax() || da.get(i) < axes[i]->getMin()))
								show = false;
						if (show)
							painter.drawPoint (toPoint (xAxis, yAxis, da));
					}
				}

				//approximation
				if (visibleCurves & DilatogramWidget::Approximation) {
					QPointF oldP;
					bool oldPset = false;
					std::list<DilatogramPoint>::iterator appIt = approximation.begin();
					while (appIt != approximation.end()) {
						DilatogramPoint da = *(appIt++);
						bool show = true;
						if (!(da.heating() && (visibleCurves & DilatogramWidget::Heating)) && !(!da.heating() && (visibleCurves & DilatogramWidget::Cooling)))
							show = false;
						for (int i = 0; i < DilatogramPoint::ParameterCount && show; i++)
							if (da.get(i) != NAN && (da.get(i) > axes[i]->getMax() || da.get(i) < axes[i]->getMin()))
								show = false;
						if (show) {
							QPointF P = toPoint (xAxis, yAxis, da);

							if (da.heating())
								painter.setPen(QPen(yAxis->getColor(Axis::HeatingColor), 2));
							else
								painter.setPen(QPen(yAxis->getColor(Axis::CoolingColor), 2));

							if (oldPset)
								painter.drawLine(oldP, P);
							else
								oldPset = true;

							oldP = P;
						} else
							oldPset = false;
					}
				}
			}
		}
	}

	//selected points
	painter.setPen(QPen(Qt::black, 6));
	for (std::list<QPointF>::iterator it = selectedPoints.begin(); it != selectedPoints.end(); it++) {
		painter.drawPoint(toPoint (xDilatogramAxes.front(), yDilatogramAxes.front(), *it));
	}
	painter.setPen(QPen(Qt::black, 1));
	for (std::list<QPointF>::iterator it = selectedPoints.begin(); it != selectedPoints.end(); it++) {
		float yreal = -curheight * yDilatogramAxes.front()->toPercent((*it).y());
		painter.drawLine(toPoint (xDilatogramAxes.front(), yDilatogramAxes.front(), *it), QPointF(curwidth + Slit, yreal));
		drawText(painter, curwidth + Slit, yreal, Qt::AlignLeft | Qt::AlignVCenter, QString::number((*it).y(), 'f', 1));
	}

	if (mouseAction == DilatogramWidget::LineSelect && !isLineDragging) {
		QPointF start = toValue (xDilatogramAxes.front(), yDilatogramAxes.front(), dragStart);
		QPointF end = toValue (xDilatogramAxes.front(), yDilatogramAxes.front(), dragEnd);
		float angle = 0;
		if (coordinateSystems == DilatogramWidget::T_E) {
			angle = (start.x() - end.x()) / (10000 * (start.y() - end.y())); // 10000 - 10mm sample size
		} else if (coordinateSystems == DilatogramWidget::E_T) {
			angle = (start.y() - end.y()) / (10000 * (start.x() - end.x())); // 10000 - 10mm sample size
		} else if (coordinateSystems == DilatogramWidget::T_t || coordinateSystems == DilatogramWidget::T_t_E_t) {
			angle = (start.y() - end.y()) / (start.x() - end.x());
		}
		drawText(painter, curwidth + Slit, -curheight, Qt::AlignLeft | Qt::AlignVCenter, QString::number(angle, 'f', 6));
	}
}

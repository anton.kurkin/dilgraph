#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "configdialog.h"

#include <QFileDialog>
#include <QFile>
#include <QPrinter>
#include <QPrintDialog>
#include <QtSvg/QSvgGenerator>

const QString MainWindow::configFileName = "config.cfg";

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	pConfig = new Config(configFileName);
	pDoc = new DilatogramDocument (pConfig);

	coordinateSystem = DilatogramWidget::T_E;
	dilatationUnit = DilatogramWidget::DilatationUnitPercent;
}

MainWindow::~MainWindow()
{
	delete pDoc;
	delete pConfig;

	delete ui;
}

void MainWindow::setDilatogramWidgetCoordinateSystem (int coordinateSystem) {
	this->coordinateSystem = coordinateSystem;
	ui->dilatogram->setCoordinateSystems (coordinateSystem, dilatationUnit);
}

void MainWindow::setDilatogramWidgetDilatationUnit (int dilatationUnit) {
	this->dilatationUnit = dilatationUnit;
	ui->dilatogram->setCoordinateSystems (coordinateSystem, dilatationUnit);
}

void MainWindow::on_actionPropertes_triggered()
{
	ConfigDialog *dialog = new ConfigDialog (pConfig, this);
	dialog->show();
}

void MainWindow::on_actionExit_triggered()
{
	QApplication::exit();
}

void MainWindow::on_actionOpen_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Load File"), "data/", tr("Files (*.*)"));
	if (fileName.isEmpty())
		return;
	pDoc->load(fileName);
	pDoc->prepare();
	pDoc->approximate();
	ui->dilatogram->setDocument(pDoc);
}

void MainWindow::on_actionSave_triggered()
{
	QString PNGfilter = tr("PNG files (*.png)");
	QString SVGfilter = tr("SVG files (*.svg)");
	QString filters = PNGfilter + ";;" + SVGfilter;
	QString selectedFilter;
	QString fileName = QFileDialog::getSaveFileName (this, tr("Save File"), "", filters, &selectedFilter);
	if (fileName.isEmpty())
		return;

	QPainter painter;
	QFileInfo fi (fileName);
	QString suffix = fi.suffix();
	if (selectedFilter == PNGfilter) {
		if (suffix != "png")
			fileName.append(".png");
		QImage image (ui->dilatogram->width(), ui->dilatogram->height(), QImage::Format_ARGB32);
		if (painter.begin (&image)) {
			ui->dilatogram->paint(painter);
			painter.end();
		}
		image.save (fileName, "PNG" );
	} else if (selectedFilter == SVGfilter) {
		if (suffix != "svg")
			fileName.append(".svg");
		QSvgGenerator generator;
		generator.setFileName(fileName);
		generator.setSize(QSize(ui->dilatogram->width(), ui->dilatogram->height()));
		generator.setViewBox(QRect(0, 0, ui->dilatogram->width(), ui->dilatogram->height()));
		generator.setTitle(tr("Dilatogram"));
		generator.setDescription(tr("Generated with DilGraph"));

		if (painter.begin(&generator)) {
			ui->dilatogram->paint(painter);
			painter.end();
		}
	}
}

void MainWindow::on_actionPrint_triggered()
{
	QPrinter printer;
	printer.setPageOrientation(QPageLayout::Orientation::Landscape);
//	QSizeF paperSize = printer.paperSize( QPrinter::DevicePixel );
//	QImage scaledImage = image.scaled( paperSize.width(), paperSize.height(), Qt::KeepAspectRatio, Qt::SmoothTransformation );
	QPrintDialog printDialog(&printer, this);
	if(printDialog.exec() == QDialog::Accepted) {
		QPainter painter;
		if (painter.begin(&printer)) {
			ui->dilatogram->paint(painter);
			painter.end();
		}
	}
}

void MainWindow::on_actionAbout_triggered()
{

}

void MainWindow::on_actionZoom_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->actionZoom->setChecked(true);
		return;
	}
	ui->actionSelect->setChecked(false);
	ui->actionLineSelect->setChecked(false);
	ui->dilatogram->setMouseAction(DilatogramWidget::Zoom);
}

void MainWindow::on_actionSelect_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->actionSelect->setChecked(true);
		return;
	}
	ui->actionZoom->setChecked(false);
	ui->actionLineSelect->setChecked(false);
	ui->dilatogram->setMouseAction(DilatogramWidget::Select);
}

void MainWindow::on_actionLineSelect_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->actionLineSelect->setChecked(true);
		return;
	}
	ui->actionSelect->setChecked(false);
	ui->actionZoom->setChecked(false);
	ui->dilatogram->setMouseAction(DilatogramWidget::LineSelect);
}

void MainWindow::on_actionHeating_triggered()
{
	changeVisibleCurves();
}

void MainWindow::on_actionCooling_triggered()
{
	changeVisibleCurves();
}

void MainWindow::on_actionExperiment_triggered()
{
	changeVisibleCurves();
}

void MainWindow::on_actionApproximation_triggered()
{
	changeVisibleCurves();
}

void MainWindow::on_actionDerivative_triggered()
{
	changeVisibleCurves();
}

void MainWindow::changeVisibleCurves()
{
	int visibleCurves = 0;
	if (ui->actionHeating->isChecked())
		visibleCurves |= DilatogramWidget::Heating;
	if (ui->actionCooling->isChecked())
		visibleCurves |= DilatogramWidget::Cooling;
	if (ui->actionExperiment->isChecked())
		visibleCurves |= DilatogramWidget::Experiments;
	if (ui->actionDerivative->isChecked())
		visibleCurves |= DilatogramWidget::Derivative;
	if (ui->actionApproximation->isChecked())
		visibleCurves |= DilatogramWidget::Approximation;
	ui->dilatogram->setVisibleCurves(visibleCurves);
}

void MainWindow::on_actionT_Ea_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->actionT_Ea->setChecked(true);
		return;
	}
	ui->actionEa_T->setChecked(false);
	ui->actionT_t->setChecked(false);
	ui->actionEa_t->setChecked(false);
	setDilatogramWidgetCoordinateSystem (DilatogramWidget::T_E);
}

void MainWindow::on_actionEa_T_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->actionEa_T->setChecked(true);
		return;
	}
	ui->actionT_Ea->setChecked(false);
	ui->actionT_t->setChecked(false);
	ui->actionEa_t->setChecked(false);
	setDilatogramWidgetCoordinateSystem (DilatogramWidget::E_T);
}

void MainWindow::on_actionT_t_triggered(bool toggledOn)
{
	if (ui->actionEa_t->isChecked()) {
		if (toggledOn)
			setDilatogramWidgetCoordinateSystem (DilatogramWidget::T_t_E_t);
		else
			setDilatogramWidgetCoordinateSystem (DilatogramWidget::E_t);
	} else {
		if (toggledOn) {
			setDilatogramWidgetCoordinateSystem (DilatogramWidget::T_t);
			ui->actionEa_T->setChecked(false);
			ui->actionT_Ea->setChecked(false);
		}
		else
			ui->actionT_t->setChecked(true);
	}
}

void MainWindow::on_actionEa_t_triggered(bool toggledOn)
{
	if (ui->actionT_t->isChecked()) {
		if (toggledOn)
			setDilatogramWidgetCoordinateSystem (DilatogramWidget::T_t_E_t);
		else
			setDilatogramWidgetCoordinateSystem (DilatogramWidget::T_t);
	} else {
		if (toggledOn) {
			setDilatogramWidgetCoordinateSystem (DilatogramWidget::E_t);
			ui->actionEa_T->setChecked(false);
			ui->actionT_Ea->setChecked(false);
		}
		else
			ui->actionEa_t->setChecked(true);
	}
}

void MainWindow::on_action_percent_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->action_percent->setChecked(true);
		return;
	}
	ui->action_um->setChecked(false);
	setDilatogramWidgetDilatationUnit (DilatogramWidget::DilatationUnitPercent);
}

void MainWindow::on_action_um_triggered(bool toggledOn)
{
	if (!toggledOn) {
		ui->action_um->setChecked(true);
		return;
	}
	ui->action_percent->setChecked(false);
	setDilatogramWidgetDilatationUnit (DilatogramWidget::DilatationUnitMicrometer);
}

#ifndef AXISDIALOG_H
#define AXISDIALOG_H

#include <QDialog>
#include <QAbstractButton>

#include "dilatogramwidget.h"
#include "axis.h"

namespace Ui {
	class AxisDialog;
}

class AxisDialog : public QDialog
{
	Q_OBJECT

public:
	explicit AxisDialog (Axis* axis, DilatogramWidget* parent);
	~AxisDialog();


private:
	Ui::AxisDialog *ui;
	Axis* axis;
	DilatogramWidget* dilatogram;

	void updateAxis();
	void loadAxis ();

private slots:
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();
	void on_buttonBox_clicked (QAbstractButton* button);

};

#endif // AXISDIALOG_H

#ifndef AXIS_H
#define AXIS_H

#include <QColor>
#include <QString>

#include "dilatogrampoint.h"

class Axis
{
public:
	enum STEPTYPES {
		Value,
		Percent
	};

	enum COLORS {
		ExperimentColor,
		HeatingColor,
		CoolingColor,
		ColorCount
	};

private:
	int type;
	float totalMax;
	float totalMin;

	float currentMax;
	float currentMin;

	float gridStepValue;
	int gridStepType;

	float gridMin;
	float gridStep;
	float gridPrescision;

	static const char* axisTitles[DilatogramPoint::ParameterCount];
	static const QColor axisColors[DilatogramPoint::ParameterCount][ColorCount];
	const float MinStep = 0.00001f;

public:
	Axis (int axisType);

	float toPercent (float value);
	float toValue (float percent);
	float interval ();

	inline void updateTotalLimits () { totalMax = currentMax; totalMin = currentMin; }
	void resetLimits ();

	void setGrid (float gridStep, int gridStepType);
	void setLimits (float currentMax, float currentMin);
	void recalcGrid ();

	QString	getTitle();
	QColor	getColor(int color);

	inline int		getType()			{ return type; }
	inline float	getMax()			{ return currentMax; }
	inline float	getMin()			{ return currentMin; }
	inline float	getGridStepValue()	{ return gridStepValue; }
	inline int		getGridStepType()	{ return gridStepType; }
	inline float	getGridStep()		{ return gridStep; }
	inline float	getGridMin()		{ return gridMin; }
	inline float	getGridMax()		{ return currentMax + MinStep; }
	inline int		getGridPrescision()	{ return gridPrescision; }
	inline float	isBigEnough()		{ return interval() > MinStep; }
};

#endif // AXIS_H

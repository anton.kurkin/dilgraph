#include "dilatogrampoint.h"

DilatogramPoint::DilatogramPoint()
{
}

void DilatogramPoint::set (float time, float temperature, float dilatationmicrometer, float dilatationpercent, float power, float ddilatation_dtime, float ddilatation_dtemperature, bool isHeating)
{
	parameters [DilatogramPoint::Time] = time;
	parameters [DilatogramPoint::Temperature] = temperature;
	parameters [DilatogramPoint::DilatationMicrometer] = dilatationmicrometer;
	parameters [DilatogramPoint::DilatationPercent] = dilatationpercent;
	parameters [DilatogramPoint::Power] = power;
	parameters [DilatogramPoint::dDilatation_dTime] = ddilatation_dtime;
	parameters [DilatogramPoint::dDilatation_dTemperature] = ddilatation_dtemperature;
	this->isHeating = isHeating;
}

#include "dilatogramdocument.h"
#include <cmath>
#include <QByteArray>
#include <list>
#include <QListIterator>
#include <QFile>
#include <QTextStream>
#include <stdexcept>
#include <iostream>
#include <QStringList>
#include <QStringListIterator>
#include <QRegularExpression>

DilatogramDocument::DilatogramDocument (Config* pConfig)
{
	this->pConfig = pConfig;

	dilatationOffset = pConfig->getMinDil();
}

int DilatogramDocument::readoutLine (QVector <float>* readoutVector, QString line, int n)
{
	readoutVector->resize (n);
	QStringList fieldsStr = line.split (QRegularExpression ("[ \\t]+"), Qt::SkipEmptyParts);
	if (fieldsStr.size() != n) return fieldsStr.size();
	QStringListIterator fieldsIt (fieldsStr);
	int i = 0;
	while (fieldsIt.hasNext()) {
		bool ok = true;
		(*readoutVector)[i] = fieldsIt.next().toFloat(&ok);
		if (!ok) return i;
		i++;
	}
	return i;
}

void DilatogramDocument::load (QString fileName)
{
	QFile file (fileName);

	if (!file.open(QFile::ReadOnly))
		return;
	readout.clear();

	for (int i = 0; i < pConfig->getSkipLines(); i++)
		if (i == pConfig->getLabelLine())
			label = file.readLine ();
		else
			file.readLine();

	int errcounter = 0;
	QVector <float> readoutPoint;

	QTextStream fin(&file);
	while ( !fin.atEnd() )
	{
		QString line = fin.readLine().trimmed();
		if (readoutLine(&readoutPoint, line, pConfig->getColN()) != pConfig->getColN()) {
			errcounter++;
			continue;
		}

		readout.push_back(std::move(readoutPoint));
	}
	file.close();
}

void DilatogramDocument::prepare ()
{
	preparedReadout.clear();

	std::list<QVector <float>>::iterator itReadout = readout.begin();
	if (itReadout == readout.end()) return;
	QVector <float> current = *(itReadout++);

	if (!pConfig->getMinDil())
		dilatationOffset = getReadoutParameter(current, pConfig->getDilatationCol()) * pConfig->getDilCoefficient();

	bool isHeating = true;
	DilatogramPoint point;
	float time = getReadoutParameter(current, pConfig->getTimeCol());
	float fixTemperature = getReadoutParameter(current, pConfig->getRealTemperatureCol());
	float fixDilatation = sensorValueToDilatation (getReadoutParameter(current, pConfig->getDilatationCol()));
	float fixPower = getReadoutParameter(current, pConfig->getPowerCol());
	point.set(time, fixTemperature, fixDilatation, dilatationMicrometerToPercent (fixDilatation), fixPower, isHeating);
	preparedReadout.push_back(point);

	while (itReadout != readout.end()) {
		current = *(itReadout++);
		time = getReadoutParameter(current, pConfig->getTimeCol());
		float temperature = getReadoutParameter(current, pConfig->getRealTemperatureCol());
		float dilatation = sensorValueToDilatation (getReadoutParameter(current, pConfig->getDilatationCol()));
		float power = getReadoutParameter(current, pConfig->getPowerCol());

		if (std::abs (dilatation - fixDilatation) > pConfig->getDilatationChangeMax())
			continue; // we have a spike here
		fixDilatation = dilatation;
		point.set(time, temperature, dilatation, dilatationMicrometerToPercent (dilatation), power, isHeating);

		if (isHeating) { // heating
			if (temperature >= fixTemperature) { //everything OK
				fixTemperature = temperature;
				preparedReadout.push_back(point);
			}
			else {
				if (std::abs (temperature - fixTemperature) > pConfig->getTemperaturePrecision()) { // temperature is much smaller so it counts as cooling start
					isHeating = false;
					fixTemperature = temperature;
					point.set(time, temperature, dilatation, dilatationMicrometerToPercent (dilatation), power, isHeating);
					preparedReadout.push_back(point);
				} else { //temperature is somewhat smaller then max so move it as a bubble at its place
					std::list<DilatogramPoint>::iterator itPrep = preparedReadout.end();
					while ((itPrep != preparedReadout.begin()) && ((*(std::prev(itPrep))).getTemperature() > temperature))
						itPrep--;
					preparedReadout.insert(itPrep, point);
				}
			}
		} else { // cooling
			if (temperature <= fixTemperature) { //everything OK
				fixTemperature = temperature;
				preparedReadout.push_back(point);
			}
			else {
				if (std::abs (temperature - fixTemperature) > pConfig->getTemperaturePrecision()) { // temperature is much higher so it counts as heating start
					isHeating = true;
					fixTemperature = temperature;
					point.set(time, temperature, dilatation, dilatationMicrometerToPercent (dilatation), power, isHeating);
					preparedReadout.push_back(point);
				} else { //temperature is somewhat bigger then max so move it as a bubble
					std::list<DilatogramPoint>::iterator itPrep = preparedReadout.end();
					while ((itPrep != preparedReadout.begin()) && ((*std::prev(itPrep)).getTemperature() < temperature))
						itPrep--;
					preparedReadout.insert(itPrep, point);
				}
			}
		}
	}
}

void DilatogramDocument::approximate ()
{
	approximation.clear();

	switch (pConfig->getApproximationMethod()) {
	case Config::MovingAverage:
		MovingSample(&DilatogramDocument::MovingAverageApproximation);
		return;
	case Config::MovingLeastSquares:
		MovingSample(&DilatogramDocument::MovingLeastSquaresApproximation);
		return;
	}
}

//takes approximation function as argument
void DilatogramDocument::MovingSample (DilatogramPoint (DilatogramDocument::*appf)(QQueue <DilatogramPoint> sample, bool *ok))
{
	std::list<DilatogramPoint>::iterator itPrep  = preparedReadout.begin();
	if (itPrep == preparedReadout.end()) return;
	DilatogramPoint point = *(itPrep++); //current (first of new temperature)

	QQueue <DilatogramPoint> sample;
	sample.enqueue(point);
	bool isHeating = point.heating();
	while (itPrep != preparedReadout.end() && (itPrep->getTemperature() == point.getTemperature()))
		sample.enqueue(*(itPrep++));

	bool ok = true;
	while (itPrep != preparedReadout.end()) {
		point = *(itPrep++);

		if (point.heating() != isHeating) { // heating changed to cooling or vice versa
			isHeating = point.heating();
			while (sample.first().getTemperature() != sample.last().getTemperature()
				   || sample.first().heating() != sample.last().heating()) {
				DilatogramPoint tmpPoint = sample.dequeue();
				while (tmpPoint.getTemperature() == sample.first().getTemperature())
					sample.dequeue();
			}
		}

		//remove points from sample until it's in temperature interval or only one temperature remains
		while (std::abs (point.getTemperature() - sample.first().getTemperature()) > pConfig->getTemperatureInterval (isHeating)
			   && sample.first().getTemperature() != sample.last().getTemperature()) {
			DilatogramPoint tmpPoint = sample.dequeue();
			while (tmpPoint.getTemperature() == sample.first().getTemperature())
				sample.dequeue();
		}

		sample.enqueue(point);
		while (itPrep != preparedReadout.end()
			   && std::abs (itPrep->getTemperature() - sample.first().getTemperature()) < pConfig->getTemperatureInterval (isHeating)
			   && isHeating == itPrep->heating()) {
			sample.enqueue(*(itPrep++));
		}
		DilatogramPoint tmpPoint = (this->*appf) (sample, &ok);
		if (ok) approximation.push_back (tmpPoint);
	}
}

DilatogramPoint DilatogramDocument::MovingLeastSquaresApproximation (QQueue <DilatogramPoint> sample, bool *ok)
{
	DilatogramPoint point;
	point.set();

	int n = sample.size();
	if (n < 2) {
		*ok = false;
		return point;
	}

	double sumTime = 0;
	double sumTemperature = 0;
	double sumDilatation = 0;
	double sumPower = 0;

	double sumDilatationTemperature = 0;
	double sumTemperatureSquared = 0;

	double sumDilatationTime = 0;
	double sumTemperatureTime = 0;
	double sumTimeSquared = 0;

	QListIterator <DilatogramPoint> itSample (sample);
	while (itSample.hasNext()) {
		DilatogramPoint tmpPoint = itSample.next();
		sumTime += tmpPoint.getTime();
		sumTemperature += tmpPoint.getTemperature();
		sumDilatation += tmpPoint.getDilatationMicrometer();
		sumPower += tmpPoint.getPower();

		sumDilatationTemperature += tmpPoint.getDilatationMicrometer() * tmpPoint.getTemperature();
		sumTemperatureSquared += tmpPoint.getTemperature() * tmpPoint.getTemperature();

		sumDilatationTime += tmpPoint.getDilatationMicrometer() * tmpPoint.getTime();
		sumTemperatureTime += tmpPoint.getTemperature() * tmpPoint.getTime();
		sumTimeSquared += tmpPoint.getTime() * tmpPoint.getTime();
	}

	if (n * sumTemperatureSquared == sumTemperature * sumTemperature || n * sumTimeSquared == sumTime * sumTime) {
		*ok = false;
		return point;
	}

	float averageTime = sumTime / n;

	//float averageTemperature = sumTemperature / n;

	//double dDilatation_dTemperature = (n * sumDilatationTemperature - sumTemperature*sumDilatation) / (n * sumTemperatureSquared - sumTemperature*sumTemperature);
	//double deltaDilatation_Temperature = (sumDilatation - dDilatation_dTemperature * sumTemperature) / n;
	//float approximateDilatation_Temperature = dDilatation_dTemperature * averageTemperature + deltaDilatation;

	double dTemperature_dTime = (n * sumTemperatureTime - sumTemperature * sumTime) / (n * sumTimeSquared - sumTime * sumTime);
	double deltaTemperature_Time = (sumTemperature - dTemperature_dTime * sumTime) / n;
	float approximateTemperature_Time = dTemperature_dTime * averageTime + deltaTemperature_Time;

	double dDilatation_dTime = (n * sumDilatationTime - sumDilatation * sumTime) / (n * sumTimeSquared - sumTime * sumTime);
	double deltaDilatation_Time = (sumDilatation - dDilatation_dTime * sumTime) / n;
	float approximateDilatation_Time = dDilatation_dTime * averageTime + deltaDilatation_Time;

	double dDilatation_dTemperature = dDilatation_dTime / dTemperature_dTime;

	float averagePower = sumPower / n;
	if (averagePower == NAN)
		averagePower = dTemperature_dTime;

	*ok = true;
	point.set(averageTime, approximateTemperature_Time, approximateDilatation_Time, dilatationMicrometerToPercent (approximateDilatation_Time), averagePower, dDilatation_dTime, dDilatation_dTemperature, sample.last().heating());
	return point;
}

DilatogramPoint DilatogramDocument::MovingAverageApproximation (QQueue <DilatogramPoint> sample, bool *ok)
{
	DilatogramPoint point;
	point.set();

	int n = sample.size();
	if (sample.isEmpty()) {
		*ok = false;
		return point;
	}

	double sumTime = 0;
	double sumTemperature = 0;
	double sumDilatation = 0;
	double sumPower = 0;

	QListIterator <DilatogramPoint> itSample (sample);
	while (itSample.hasNext()) {
		DilatogramPoint tmpPoint = itSample.next();
		sumTime += tmpPoint.getTime();
		sumTemperature += tmpPoint.getTemperature();
		sumDilatation += tmpPoint.getDilatationMicrometer();
		sumPower += tmpPoint.getPower();
	}

	float averageTime = sumTime / n;
	float averageTemperature = sumTemperature / n;
	float averageDilatation = sumDilatation / n;
	float averagePower = sumPower / n;

	*ok = true;
	point.set(averageTime, averageTemperature, averageDilatation, dilatationMicrometerToPercent (averageDilatation), averagePower, sample.last().heating());
	return point;
}

void DilatogramDocument::save (QString fileName)
{
	QFile file (fileName);
	if (!file.open(QFile::WriteOnly))
		return;

	file.close();
}

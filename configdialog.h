#ifndef PROPERTIESDIALOG_H
#define PROPERTIESDIALOG_H

#include <QDialog>
#include <QAbstractButton>
#include "config.h"


namespace Ui {
	class ConfigDialog;
}

class ConfigDialog : public QDialog
{
	Q_OBJECT

public:
	explicit ConfigDialog(Config* pConfig, QWidget *parent = 0);
	~ConfigDialog();
	void loadConfig ();
	void updateConfig ();

private slots:
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();
	void on_buttonBox_clicked (QAbstractButton* button);

private:
	Ui::ConfigDialog *ui;
	Config* pConfig;
};

#endif // PROPERTIESDIALOG_H

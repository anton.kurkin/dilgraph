#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QChar>
#include <QHash>

class Config
{
public:
	enum {
		//Readout
		SkipLines,
		LabelLine,
		ColN,
		TimeCol,
		PlannedTemperatureCol,
		RealTemperatureCol,
		DilatationCol,
		PowerCol,

		//Preparation
		DilCoefficient,
		MinDil,
		TemperaturePrecision,
		DilatationChangeMax,
		SampleSize,

		//Approximation
		ApproximationMethod,
		TemperatureIntervalCooling,
		TemperatureIntervalHeating,

		PropertiesTotal
	} PROPERTIES;

	enum  {
		MovingLeastSquares,
		MovingAverage
	} APPROXIMATIONMETHOD;

	enum {
		Readout,
		Preparation,
		Approximation,
		PropertySubsectionTotal
	} PROPSUBSECTIONS;

	static const int PropertySubsectionCodes[PropertySubsectionTotal];

private:
	union ConfigPropertyValue {
		float FloatValue;
		int IntValue;
	};

	enum {
		Float,
		Int
	} PROPTYPE;

	struct PropertyDescriptor {
		QString  name;
		int type;
		int subsection;
	};

	static const PropertyDescriptor PropertyDescriptors[PropertiesTotal];
	static QHash <QString, int> PropertyNameIndex;

	static const QString PropertySubsectionNames[PropertySubsectionTotal];

	enum {
		ReadoutUpdate = 0x1,
		PreparationUpdate = 0x2,
		ApproximationUpdate = 0x4
	} PROPSUBSECTIONUPDATES;
	static const char separator;
	static const char assignment;

	QString fileName;

	static class _init
	{
	public:
		_init() { for(int i = 0; i < PropertiesTotal; i++)
					PropertyNameIndex.insert(PropertyDescriptors[i].name, i);}
	} _initializer;

	ConfigPropertyValue propertyValues[PropertiesTotal];
	int updatedFlags;

	void initDefault ();
	bool setValue (int property, int value);
	bool setValue (int property, float value);

public:
	Config ();
	void load ();
	void save ();
	Config (QString fileName);
	void load (QString fileName);
	void save (QString fileName);

	void setReadoutParms (int skipLines, int labelLine, int colN, int timeCol, int plannedTemperatureCol, int realTemperatureCol, int dilatationCol, int powerCol);
	void setPreparationParms (float dilCoeff, float minDil, float temperaturePrecision, float dilatationChangeMax, float sampleSize);
	void setApproximationParms (int approximationMethod, float coolingTemperatureInterval, float heatingTemperatureInterval);

	//Readout
	inline int getSkipLines () {return propertyValues[Config::SkipLines].IntValue;}
	inline int getLabelLine () {return propertyValues[Config::LabelLine].IntValue;}
	inline int getColN () {return propertyValues[Config::ColN].IntValue;}
	inline int getTimeCol () {return propertyValues[Config::TimeCol].IntValue;}
	inline int getPlannedTemperatureCol () {return propertyValues[Config::PlannedTemperatureCol].IntValue;}
	inline int getRealTemperatureCol () {return propertyValues[Config::RealTemperatureCol].IntValue;}
	inline int getDilatationCol () {return propertyValues[Config::DilatationCol].IntValue;}
	inline int getPowerCol () {return propertyValues[Config::PowerCol].IntValue;}
	//Preparation
	inline float getDilCoefficient () {return propertyValues[Config::DilCoefficient].FloatValue;}
	inline float getMinDil () {return propertyValues[Config::MinDil].FloatValue;}
	inline float getTemperaturePrecision () {return propertyValues[Config::TemperaturePrecision].FloatValue;}
	inline float getDilatationChangeMax () {return propertyValues[Config::DilatationChangeMax].FloatValue;}
	inline float getSampleSize () {return propertyValues[Config::SampleSize].FloatValue;}
	//Approximation
	inline int getApproximationMethod () {return propertyValues[Config::ApproximationMethod].IntValue;}
	inline float getTemperatureIntervalCooling () {return propertyValues[Config::TemperatureIntervalCooling].FloatValue;}
	inline float getTemperatureIntervalHeating () {return propertyValues[Config::TemperatureIntervalHeating].FloatValue;}

	inline float getTemperatureInterval (int state) {return propertyValues[Config::TemperatureIntervalCooling + state].FloatValue;}

	inline void updatesApplied () {updatedFlags = 0;}
	inline int updates () {return updatedFlags;}
};

#endif // CONFIG_H
